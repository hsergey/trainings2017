/// 2.28 Program add space between numbersi
#include <iostream>

int 
main()
{
    int fiveDigit;
    std::cout << "Input five digit interger: ";
    std::cin >> fiveDigit;
    if (fiveDigit / 10000 > 10) {
        std::cout << "Error 1: You have entered more then five digit number \n        Program work only for five digit number" << std::endl;
        return 1;
    }
    int fifth = fiveDigit / 10000;
    if (0 == fifth) {
         std::cout << "Error 1: You have entered less then five  digit number \n        Program work only for five digit number" << std::endl; 
        return 1;
    }
    int fourth = fiveDigit % 10000 / 1000;
    int third  = fiveDigit % 1000 / 100;
    int second = fiveDigit % 100 / 10;
    int fist = fiveDigit % 10 / 1;
    std::cout << fifth << " " << fourth << " " << third << " " << second << " " << fist << std::endl;
    return 0;
}
