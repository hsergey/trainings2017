/// 2.24 Program determine odd and even 
#include <iostream>

int 
main()
{
    int a;
    std::cin >> a;
    if (0 == a) {
        std::cout << "Error 1: Division by 0" << std::endl;
        return 1;
    }
    int module = a % 2; 
    if (0 == module) { 
        std::cout << "Input number is even" << std::endl;
        return 0;
    }
    std::cout << "Input number is odd" << std::endl;
    return 0;
}
