/// 2.23 Program Calculate maximum and minimum for 5 numbers
#include <iostream>

int 
main()
{
    int a, b, c, d, e;
    std::cout << "Enter 5 numbers: " << std::endl; 
    std::cin >> a >> b >> c >> d >> e;
    
    /// smallest
    int smallest = a;
    if (b < smallest) { 
        smallest = b;
    }
    if (c < smallest) {
        smallest = c;
    }
    if (d < smallest) {
        smallest = d;
    }
    if (e < smallest) {
        smallest = e;
    }
    std::cout << "smallest number is " << smallest << std::endl;
    
    /// largesst
    int largest = a;
    if (b > largest) { 
        largest = b;
    }
    if (c > largest) {
        largest = c;
    }
    if (d > largest) {
        largest = d;
    }
    if (e > largest) {
        largest = e;
    } 
    std::cout << "lergest number is " << largest << std::endl;
    return 0;
}
