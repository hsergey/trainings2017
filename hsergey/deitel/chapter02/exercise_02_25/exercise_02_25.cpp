///2.25 Program determine Entered fist number multiple to second
#include <iostream>

int 
main()
{
    int a, b;
    std::cin >> a >> b;
    if (0 == b) {
        std::cout << "Error 1: Division by 0" << std::endl;
        return 1;
    }
    int module = a % b;
    if (0 == module) {
        std::cout << "fist is a multiple for second " << std::endl;
        return 0;
    }
        std::cout << "fist is NOT a multiple for second" << std::endl; 
    return 0;
}
