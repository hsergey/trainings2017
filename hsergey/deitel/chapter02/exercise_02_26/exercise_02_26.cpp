/// 2.21 Program draw chessBoarde
#include <iostream>
int
main()
{
    std::cout << "* * * * * * * * " << std::endl; 
    std::cout << " * * * * * * * *" << std::endl; 
    std::cout << "* * * * * * * * " << std::endl; 
    std::cout << " * * * * * * * *" << std::endl; 
    std::cout << "* * * * * * * * " << std::endl; 
    std::cout << " * * * * * * * *" << std::endl; 
    std::cout << "* * * * * * * * " << std::endl; 
    std::cout << " * * * * * * * *" << std::endl; 
    std::cout << "\n\n\n " << std::endl; 
    std::cout << "* * * * * * * * \n"
                 " * * * * * * * *\n"
                 "* * * * * * * * \n"
                 " * * * * * * * *\n"
                 "* * * * * * * * \n"
                 " * * * * * * * *\n"
                 "* * * * * * * * \n"
                 " * * * * * * * *\n"; /// With minimum stamment 
    return 0;
}
