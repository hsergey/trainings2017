/// Program calculate sum, product,difference and quotient for two numbers.
#include <iostream>
int 
main()
{
    int a;
    int b;
    std::cout << "Type two integers:" << std::endl;
    std::cin >> a >> b;
    if (0 == b) {
        std::cout << "Error 1: Division by 0" << std::endl;
        return 1;
    }
    std::cout << "a + b = " << (a + b) << std::endl;
    std::cout << "a - b = " << (a - b) << std::endl;
    std::cout << "a * b = " << (a * b) << std::endl;
    std::cout << "a / b = " << (a / b) << std::endl;
    std::cout << "a % b = " << (a % b) << std::endl;
    return 0;
}
