// 2.21 Program draw shapese
#include <iostream>

int 
main()
{
    std::cout << "*********" << "\t" << "  ***  " << "\t" << "  *  " << "\t" << "    *    " << std::endl; 
    std::cout << "*       *" << "\t" << " *   * " << "\t" << " *** " << "\t" << "   * *   " << std::endl; 
    std::cout << "*       *" << "\t" << "*     *" << "\t" << "*****" << "\t" << "  *   *  " << std::endl; 
    std::cout << "*       *" << "\t" << "*     *" << "\t" << "  *  " << "\t" << " *     * " << std::endl; 
    std::cout << "*       *" << "\t" << "*     *" << "\t" << "  *  " << "\t" << "*       *" << std::endl; 
    std::cout << "*       *" << "\t" << "*     *" << "\t" << "  *  " << "\t" << " *     * " << std::endl; 
    std::cout << "*       *" << "\t" << "*     *" << "\t" << "  *  " << "\t" << "  *   *  " << std::endl; 
    std::cout << "*       *" << "\t" << " *   * " << "\t" << "  *  " << "\t" << "   * *   " << std::endl; 
    std::cout << "*********" << "\t" << "  ***  " << "\t" << "  *  " << "\t" << "    *    " << std::endl;  
    return 0;
}
