/// Program print 1-4 numbers
#include <iostream>
int 
main()
{
    std::cout << "1 2 3 4 \n"; /// One statment with one stream insertionn
    std::cout << "1 " << "2 " << "3 " << "4\n"; /// One statement with 4 stream insertion
    /// Four statments
    std::cout << "1 ";
    std::cout << "2 ";
    std::cout << "3 ";
    std::cout << "4\n";
    /// End four statment
    return 0;
}
