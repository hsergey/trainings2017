/// 2.20 Program calculate circuit diameter circumference and area
#include <iostream>
int 
main()
{
    int radius;
    std::cout << "Enter circuit radius" <<  std::endl; 
    std::cin >> radius;
    if (radius < 0){
        std::cout << "Please enter positive number" << std::endl;
        return 1;
    }
    std::cout << "diameter is " << 2 * radius << std::endl; 
    std::cout << "circumference is " << 2 * 3.14159 * radius << std::endl; 
    std::cout << "area is " << 3.14159 * radius * radius << std::endl; 
    return 0;
}
